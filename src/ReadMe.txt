MTA Monitored Actor
- Built on the MGI Monitored Actor, with added RADE LOGGER and circulating Message tracking. 
- Should be placed inside the vi.lib directory to work with the Inheritance Switcher tool.

MTA Actor Tokens: 
Add and enable these token in the project Conditional Disable symbols. (Note: these are case sensistive)

mtaActorDebug = true //General enable for the all functionalities
mtaActorPopupError = true //In case of error, will fidplsy an error popup slider (if target is not RT)
mtaActorLogMsg =true //In case of error, sends the description to the RADE Logger (or console, in the case of RT targets)